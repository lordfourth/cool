package com.example.tinyurl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class UrlController {

    @Autowired
    private UrlRepo urlrepo;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public List<Url> findall() {
        return urlrepo.findAll();
    }


    @ResponseBody
    @RequestMapping(value = "/tinyurl/{url}", method = RequestMethod.GET)
    public String findbyUrl(@PathVariable("url") String url){
        Url res = urlrepo.findByUrl(url);
        if(res==null)return "Url not registered";
        else
        return res.getId();
    }

    @ResponseBody
    @RequestMapping(value = "/url/{id}", method = RequestMethod.GET)
    public String findbyTinyUrl(@PathVariable("id") String url){
        return urlrepo.findByid(url).getUrl();
    }

    @ResponseBody
    @RequestMapping(value = "/post/{url}", method = RequestMethod.POST)
    public String posturl(@PathVariable("url") String url){
        if(urlrepo.findByUrl(url)!=null)return "Url already registered";
        Url res = new Url(url);
        urlrepo.save(res);
        return "Success"+" Tinyurl for url "+url+" is "+ res.getId();
    }


}
