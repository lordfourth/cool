package com.example.tinyurl;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UrlRepo extends MongoRepository<Url, String>{
     Url findByUrl(String url);
     Url findByid(String id);
}
