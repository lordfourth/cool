package com.example.tinyurl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class TinyurlApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	private UrlRepo repository;

	public static void main(String[] args) {
		SpringApplication.run(TinyurlApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		//repository.deleteAll();
		//repository.save(new Url("a.com"));
		//repository.save(new Url("ss.com"));
		//repository.save(new Url("q.com"));
		//repository.save(new Url("w.com"));
		//Url tmp = repository.findByUrl("googe.com");
		//System.out.println(tmp);


	}

}
