package com.example.tinyurl;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
public class Url {

    @Indexed(unique = true)
    public String url;
    @Id
    public String id;

    public String getId() {
        return this.id;
    }

    public String getUrl() {
        return this.url;
    }

    public Url() {
    }

    public Url(String a) {
        this.url = a;
    }
}